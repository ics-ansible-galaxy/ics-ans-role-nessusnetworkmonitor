# ics-ans-role-nessusnetworkmonitor

Ansible role to install nessusnetworkmonitor.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-nessusnetworkmonitor
```

## License

BSD 2-clause
