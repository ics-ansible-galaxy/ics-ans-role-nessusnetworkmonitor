import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_port_and_service(host):
    test_port = host.socket("tcp://0.0.0.0:8835").is_listening
    test_service1 = host.service("nnm-proxy.service")
    test_service2 = host.service("nnm.service")
    assert test_service1
    assert test_service2
    assert test_port
